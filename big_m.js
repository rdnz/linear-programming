"use strict";
const lib = require("./runtime_environment/library_node");

// modifies original argument
const basic_variable = function(tableau) {
  for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
    tableau[i].basic_variable = undefined;
  columns_iterate: for (let j = tableau[0].length - 2; j >= 1; --j) { // exclude right hand side column and objective column
    let i = 0;
    // find positive constraint coefficient
    for (;;) {
      if (i >= tableau.length - 1) // exclude objective row
        continue columns_iterate;
      const relation_zero = lib.m_compare(tableau[i][j], 0);
      if (relation_zero > 0)
        break;
      ++i;
      if (relation_zero == 0)
        continue;
      else // tableau[i][j] < 0
        continue columns_iterate;
    }
    if (tableau[i].basic_variable !== undefined)
      continue columns_iterate;
    const basic_row = i;
    // rule out further non-zero constraint coefficients omitting objective row
    for (++i; i < tableau.length - 1; ++i) // exclude objective row
      if (!lib.m_equal(tableau[i][j], 0))
        continue columns_iterate;
    // save basic variable coefficients
    tableau[basic_row].basic_variable = j;
    tableau[basic_row].basic_variable_constraint_coeff = tableau[basic_row][j];
    tableau[basic_row].basic_variable_objective_coeff =
      lib.array_last(tableau)[j];
  }
  return tableau;
};
// modifies original argument
const big_m = function(tableau, log) {
  // tableau = tableau.slice();
  // for (let i = 0; i < tableau.length; ++i)
  //   tableau[i] = tableau[i].slice();
  if (log === undefined)
    log = true;
  lib.print("Big M input", log);
  lib.tableau_print(tableau, {}, [], log);
  // make right hand sides positive
  for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
    if (lib.m_less_than(lib.array_last(tableau[i]), 0))
      tableau = lib.tableau_row_multiplication(tableau, i, -1, log);
  lib.print("Make right hand sides positive.", log);
  lib.tableau_print(tableau, {}, tableau[0].length - 1, log);
  // search for basic variables
  tableau = basic_variable(tableau);
  if (log) {
    const basic_variable_list = [];
    for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
      if (tableau[i].basic_variable !== undefined)
        basic_variable_list.push(tableau[i].basic_variable);
    lib.print("Search for basic variables.");
    lib.tableau_print(tableau, {}, basic_variable_list);
  }
  // count artificial variable to be added
  let additional_variable_count = 0;
  if (log) {
    for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row_index
      if (tableau[i].basic_variable === undefined)
        ++additional_variable_count;
  }
  // introduce artificial variables
  const artificial_variable = [];
  let j = tableau[0].length - 1;
  basic_variable_iterate: for (let i = tableau.length - 2; i >= 0; --i) { // exclude objective row
    if (tableau[i].basic_variable === undefined) {
      // insert artificial variable column
      tableau[i].basic_variable = j;
      let i2;
      for (i2 = 0; i2 < tableau.length - 1; ++i2) // exclude objective row
        tableau[i2].splice(j, 0, lib.m(i2 == i ? 1 : 0));
      tableau[i].basic_variable_constraint_coeff = lib.m(1);
      tableau[i2].splice(j, 0, lib.m("M")); // objective row
      tableau[i].basic_variable_objective_coeff = lib.m("M");
      if (log) {
        --additional_variable_count;
        artificial_variable.push(j + additional_variable_count); // correct index shifts of splice
      }
    }
    // adjust column index for next insertion
    for (j; j >= 1; --j) {
      for (let i2 = 0; i2 < tableau.length - 1; ++i2) // exclude objective row
        if (!(
          i2!=i && lib.m_equal(tableau[i2][j-1], 0) ||
          i2==i && !lib.m_equal(tableau[i2][j-1], 0)
        ))
          continue basic_variable_iterate;
    }
  }
  lib.print("Introduce artificial variables.", log);
  lib.tableau_print(tableau, {}, artificial_variable, log);
  // make basic variable objective coefficients zero
  lib.print("Make objective coefficients of basic variable zero.", log);
  for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
    if (!lib.m_equal(tableau[i].basic_variable_objective_coeff, 0)) {
      if (log) {
        const annotation = {};
        annotation[i] = " → First, ";
        const action = [];
        if (!lib.m_equal(tableau[i].basic_variable_objective_coeff, 1))
          action.push("multiply by " +
            lib.m_string(tableau[i].basic_variable_objective_coeff));
        if (!lib.m_equal(tableau[i].basic_variable_constraint_coeff, 1))
          action.push("divide by " +
            lib.m_string(tableau[i].basic_variable_constraint_coeff));
        annotation[i] += action.join(" and ") + ".";
        annotation[tableau.length - 1] = " ← Second, substract from this row.";
        lib.tableau_print(tableau, annotation);
      }
      for (let j = 0; j < lib.array_last(tableau).length; ++j)
        lib.array_last(tableau)[j] =
          tableau[i][j]
            .mul(tableau[i].basic_variable_objective_coeff)
            .div(tableau[i].basic_variable_constraint_coeff)
            .neg()
            .add(lib.array_last(tableau)[j]);
    }
  return tableau;
};
// modifies original argument
const simplex = function(tableau, log) {
  // tableau = tableau.slice();
  // for (let i = 0; i < tableau.length; ++i)
  //   tableau[i] = tableau[i].slice();
  if (log === undefined)
    log = true;
  for (let finite_loop = 0; finite_loop < 50; ++finite_loop) {
    // determine pivot column
    let pivot_column = 0;
    for (let i = 1; i < lib.array_last(tableau).length - 1; ++i) // exclude right hand side
      if (lib.m_less_than(lib.array_last(tableau)[i], lib.array_last(tableau)[pivot_column]))
        pivot_column = i;
    // determine if done
    if (lib.m_less_than_equal(0, lib.array_last(tableau)[pivot_column])) {
      if (log) {
        tableau = basic_variable(tableau);
        const basic_variable_list = [];
        for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
          if (tableau[i].basic_variable !== undefined)
            basic_variable_list.push(tableau[i].basic_variable);
        basic_variable_list.push(lib.array_last(tableau).length - 1);
        lib.print("Done.", log);
        lib.tableau_print(tableau, tableau.length - 1, basic_variable_list, log);
      }
      return tableau;
    }
    lib.print("Determine pivot column.", log);
    lib.tableau_print(tableau, {}, pivot_column, log);
    // determine pivot row
    const annotation = {};
    let pivot_row = undefined;
    for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
      if (!lib.m_equal(tableau[i][pivot_column], 0)) {
        annotation[i] = lib.array_last(tableau[i]).div(tableau[i][pivot_column]);
        if (pivot_row === undefined || lib.m_less_than(
          annotation[i],
          lib.array_last(tableau[pivot_row]).div(tableau[pivot_row][pivot_column])
        ))
          pivot_row = i;
        if (log)
          annotation[i] =
            " " + lib.m_string(lib.array_last(tableau[i])) +
            " / " + lib.m_string(tableau[i][pivot_column]) +
            " = " + annotation[i];
      }
    if (log)
      annotation[pivot_row] += " ←";
    lib.print("Determin pivot row.", log);
    lib.tableau_print(tableau, annotation, pivot_column, log);
    // if (pivot_row === undefined) // colomn with zeros only
    // normalize pivot row
    const pivot_value = tableau[pivot_row][pivot_column];
    for (let j = 0; j < tableau[pivot_row].length; ++j)
      tableau[pivot_row][j] = tableau[pivot_row][j].div(pivot_value);
    lib.print("Normalize pivot row.", log);
    lib.tableau_print(tableau, pivot_row, pivot_column, log);
    // elementary row operations
    lib.print("Elementary row operations.", log);
    for (let i = 0; i < tableau.length; ++i)
      if (i != pivot_row)
        tableau = lib.tableau_row_addition(
          tableau,
          i, // target row
          pivot_row, // source row
          tableau[i][pivot_column].neg(), // factor
          log
        );
  }
};
const solution_exists = function(tableau, log) {
  if (log === undefined)
    log = true;
  for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
    if (lib.m_less_than(lib.array_last(tableau[i]), 0)) {
      lib.print("Solution does not exist.", log);
      lib.tableau_print(tableau, i, tableau.length - 1, log);
      return false;
    }
  if (log) {
    lib.print("Solution exists.");
    const annotation = [];
    for (let i = 0; i < tableau.length - 1; ++i) // exclude objective row
      annotation.push(i);
    lib.tableau_print(tableau, annotation, tableau[0].length - 1);
  }
  return true;
};

// let tableau = lib.tableau([
//   [0, 2, 3,  2,1,0,1000],
//   [0, 1, 1,  2,0,1, 800],
//   [1,-7,-8,-10,0,0,   0]
// ]);
let tableau = lib.tableau([
  [0, 1,1, 0,1, 0,20],
  [0, 1,0, 1,0, 0, 5],
  [0, 0,1, 1,0,-1,10],
  [1,-1,1,-3,0, 0, 0]
]);
const log = true;
tableau = big_m(tableau, log);
if (solution_exists(tableau, log))
  tableau = simplex(tableau, log);
