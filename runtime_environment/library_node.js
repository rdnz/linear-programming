"use strict";
const file_system = require("fs");
let preserve_this = (object, method) => object[method].bind(object);
let library = {};
library.prompt_gui = function(question, title, answer_default) {
  return this.prompt_lui(question + (
    answer_default !== undefined ?
      " (Suggestion: " + answer_default + ")" :
      ""
  ));
};
library.stdin_content = function() {
  let result;
  let unanswered = true;
  let stdin_interface = require("readline").createInterface({
    "input": process.stdin
  });
  stdin_interface.question("", (answer) => {
    result = answer;
    unanswered = false;
    stdin_interface.close();
  });
  require("deasync").loopWhile(() => unanswered);
  return result;
};
library.print = function(text, log) {
  if (log !== false)
    console.log(text);
};
library.print_no_newline = preserve_this(process.stdout, "write");
library.exit = preserve_this(process, "exit");

library = Object.assign(require("./library_portable"), library);

library.Polynomial = require("./polynomial");

module.exports = library;
