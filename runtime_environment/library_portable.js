"use strict";
var rdnz_library_portable = {};
rdnz_library_portable.m_compare = function(m1, m2) {
  m1 = new this.Polynomial(m1);
  m2 = new this.Polynomial(m2);
  for (var i = Math.max(m1.degree(), m2.degree()); i >= 0; --i) {
    var result =
      (i.toString() in m1.coeff ? m1.coeff[i] : 0) -
      (i.toString() in m2.coeff ? m2.coeff[i] : 0);
    if (result)
      return result;
  }
  return 0;
};
rdnz_library_portable.m_less_than = function(m1, m2) {
  return this.m_compare(m1, m2) < 0;
};
rdnz_library_portable.m_less_than_equal = function(m1, m2) {
  return this.m_compare(m1, m2) <= 0;
};
rdnz_library_portable.m_equal = function(m1, m2) {
  if (m1 === 0)
    return (m2.degree() === -Infinity);
  if (m2 === 0)
    return (m1.degree() === -Infinity);
  return this.m_compare(m1, m2) == 0;
};
rdnz_library_portable.m = function(m) {
  if (typeof m == "string")
    m = m.replace(" ", "").replace(/m/ig, "x");
  return (new this.Polynomial(m));
};
// modifies original argument
rdnz_library_portable.tableau = function(matrix) {
  // matrix = matrix.slice();
  // for (var i = 0; i < matrix.length; ++i)
  //   matrix[i] = matrix[i].slice();
  for (var i = 0; i < matrix.length; ++i)
    for (var j = 0; j < matrix[i].length; ++j)
      matrix[i][j] = this.m(matrix[i][j]);
  return matrix;
};
// modifies original argument
rdnz_library_portable.tableau_row_addition =
  function(tableau, target_row, source_row, scalar, log) {
    if (scalar == 0)
      return tableau;
    // tableau = tableau.slice();
    // for (var i = 0; i < tableau.length; ++i)
    //   tableau[i] = tableau[i].slice();
    if (scalar === undefined)
      scalar = 1;
    if(log !== false) {
      var annotation = {};
      annotation[target_row] = " ← Second, add to this row.";
      annotation[source_row] =
        " → First, multiply by " + this.m_string(scalar) + ".";
      this.tableau_print(tableau, annotation);
    }
    for (var i = 0; i < tableau[target_row].length; ++i)
      tableau[target_row][i] = tableau[source_row][i]
        .mul(scalar)
        .add(tableau[target_row][i]);
    return tableau;
  };
// modifies original argument
rdnz_library_portable.tableau_row_multiplication =
  function(tableau, row_index, scalar, log) {
    if (scalar == 1 || scalar === undefined)
      return tableau;
    // tableau = tableau.slice();
    // for (var i = 0; i < tableau.length; ++i)
    //   tableau[i] = tableau[i].slice();
    if(log !== false) {
      var annotation = {};
      annotation[row_index] = " ← Multiply by " + this.m_string(scalar) + ".";
      this.tableau_print(tableau, annotation);
    }
    for (var i = 0; i < tableau[row_index].length; ++i)
      tableau[row_index][i] = tableau[row_index][i].mul(scalar);
    return tableau;
  };
rdnz_library_portable.tableau_print =
  function(tableau, arrow_row, arrow_column, log) {
    if (log === false)
      return;
    // if (width !== undefined)
    //   this.matrix_print(tableau, this.m_string, width, arrow1_index, arrow2_index);
    // else {
    tableau = tableau.slice();
    for (var i = 0; i < tableau.length; ++i)
      tableau[i] = tableau[i].slice();
    var width_maximum = -Infinity;
    for (var i = 0; i < tableau.length; ++i)
      for (var j = 0; j < tableau[i].length; ++j) {
        tableau[i][j] = this.m_string(tableau[i][j]);
        if (tableau[i][j].length > width_maximum)
          width_maximum = tableau[i][j].length;
      }
    ++width_maximum;
    this.matrix_print(tableau, function(text) {
      return text;
    }, width_maximum, arrow_row, arrow_column);
    // }
  };
rdnz_library_portable.matrix_print =
  function(matrix, callback, width, arrow_row, arrow_column) {
    var annotation_default_row = " ←";
    if (typeof arrow_row == "number") {
      var arrow_row_number = arrow_row;
      arrow_row = {};
      arrow_row[arrow_row_number] = annotation_default_row;
    }
    else if (Array.isArray(arrow_row)) {
      var arrow_row_array = arrow_row;
      arrow_row = {};
      for (var i = 0; i < arrow_row_array.length; ++i)
        arrow_row[arrow_row_array[i]] = annotation_default_row;
    }
    if (typeof arrow_column == "number")
      arrow_column = [arrow_column];
    var output = "";
    if (arrow_column !== undefined && arrow_column.length > 0) {
      output += " ";
      for (var j = 0; j < matrix[0].length; ++j)
        output += this.pad(
          undefined !== arrow_column.find(function(column) {
            return (column == j);
          }) ? "↓" : "",
          width
        );
      output += "\n";
    }
    for (var i = 0; i < matrix.length; ++i) {
      output += "|";
      for (var j = 0; j < matrix[i].length; ++j)
        output += this.pad(callback(matrix[i][j]), width);
      output += "|";
      if (arrow_row !== undefined && i.toString() in arrow_row)
        output += arrow_row[i];
      output += "\n";
    }
    this.print(output);
  };
rdnz_library_portable.pad = function(text, width) {
  while (text.length < width)
    text = " " + text;
  return text;
};
rdnz_library_portable.m_string = function(m, width) {
  return m.toString().replace("x", "M");
};
rdnz_library_portable.prompt_lui = function(question, title, answer_default) {
  if (question === undefined)
    question = "";
  else
    question += " ";
  if (answer_default !== undefined)
    question += "[" + answer_default + "] ";
  this.print_no_newline(question);
  var answer = this.stdin_content();
  if (answer == "" && answer_default !== undefined)
    return answer_default;
  else
    return answer;
};
rdnz_library_portable.array_last = function(array, value) {
  if (value !== undefined)
    array[array.length - 1] = value;
  return array[array.length - 1];
};
if (!Array.prototype.sort)
  Array.prototype.sort = function(compare) {
    for (var i = 1; i < this.length; ++i)
      for (var j = i - 1; j >= 0; --j)
        if (compare(this[j], this[j + 1]) > 0) {
          var tmp = this[j];
          this[j] = this[j + 1];
          this[j + 1] = tmp;
        }
        else
          break;
  };
if (!Array.prototype.find)
  Array.prototype.find = function(predicate) {
    for (var i = 0; i < this.length; ++i) {
      if (predicate.call(arguments[1], this[i], i, this))
        return this[i];
    }
    return undefined;
  };

module.exports = rdnz_library_portable;
