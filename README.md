# Linear Programming

Implements the Big M method and the simplex algorithm for Node.js. Provides a logging option to help students learn the algorithm. An example output follows.

```
Big M input
|  0  1  1  0  1  0 20|
|  0  1  0  1  0  0  5|
|  0  0  1  1  0 -1 10|
|  1 -1  1 -3  0  0  0|

Make right hand sides positive.
                     ↓
|  0  1  1  0  1  0 20|
|  0  1  0  1  0  0  5|
|  0  0  1  1  0 -1 10|
|  1 -1  1 -3  0  0  0|

Search for basic variables.
               ↓      
|  0  1  1  0  1  0 20|
|  0  1  0  1  0  0  5|
|  0  0  1  1  0 -1 10|
|  1 -1  1 -3  0  0  0|

Introduce artificial variables.
                  ↓     ↓   
|  0  1  1  0  1  0  0  0 20|
|  0  1  0  1  0  1  0  0  5|
|  0  0  1  1  0  0 -1  1 10|
|  1 -1  1 -3  0  M  0  M  0|

Make objective coefficients of basic variable zero.
|  0  1  1  0  1  0  0  0 20|
|  0  1  0  1  0  1  0  0  5| → First, multiply by M.
|  0  0  1  1  0  0 -1  1 10|
|  1 -1  1 -3  0  M  0  M  0| ← Second, substract from this row.

|    0    1    1    0    1    0    0    0   20|
|    0    1    0    1    0    1    0    0    5|
|    0    0    1    1    0    0   -1    1   10| → First, multiply by M.
|    1 -M-1    1 -M-3    0    0    0    M  -5M| ← Second, substract from this row.

Solution exists.
                                                      ↓
|     0     1     1     0     1     0     0     0    20| ←
|     0     1     0     1     0     1     0     0     5| ←
|     0     0     1     1     0     0    -1     1    10| ←
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M|

Determine pivot column.
                        ↓                              
|     0     1     1     0     1     0     0     0    20|
|     0     1     0     1     0     1     0     0     5|
|     0     0     1     1     0     0    -1     1    10|
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M|

Determin pivot row.
                        ↓                              
|     0     1     1     0     1     0     0     0    20|
|     0     1     0     1     0     1     0     0     5| 5 / 1 = 5 ←
|     0     0     1     1     0     0    -1     1    10| 10 / 1 = 10
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M|

Normalize pivot row.
                        ↓                              
|     0     1     1     0     1     0     0     0    20|
|     0     1     0     1     0     1     0     0     5| ←
|     0     0     1     1     0     0    -1     1    10|
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M|

Elementary row operations.
|     0     1     1     0     1     0     0     0    20|
|     0     1     0     1     0     1     0     0     5| → First, multiply by -1.
|     0     0     1     1     0     0    -1     1    10| ← Second, add to this row.
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M|

|     0     1     1     0     1     0     0     0    20|
|     0     1     0     1     0     1     0     0     5| → First, multiply by 2M+3.
|     0    -1     1     0     0    -1    -1     1     5|
|     1  -M-1  -M+1 -2M-3     0     0     M     0  -15M| ← Second, add to this row.

Determine pivot column.
                     ↓                                          
|      0      1      1      0      1      0      0      0     20|
|      0      1      0      1      0      1      0      0      5|
|      0     -1      1      0      0     -1     -1      1      5|
|      1    M+2   -M+1      0      0   2M+3      M      0 -5M+15|

Determin pivot row.
                     ↓                                          
|      0      1      1      0      1      0      0      0     20| 20 / 1 = 20
|      0      1      0      1      0      1      0      0      5|
|      0     -1      1      0      0     -1     -1      1      5| 5 / 1 = 5 ←
|      1    M+2   -M+1      0      0   2M+3      M      0 -5M+15|

Normalize pivot row.
                     ↓                                          
|      0      1      1      0      1      0      0      0     20|
|      0      1      0      1      0      1      0      0      5|
|      0     -1      1      0      0     -1     -1      1      5| ←
|      1    M+2   -M+1      0      0   2M+3      M      0 -5M+15|

Elementary row operations.
|      0      1      1      0      1      0      0      0     20| ← Second, add to this row.
|      0      1      0      1      0      1      0      0      5|
|      0     -1      1      0      0     -1     -1      1      5| → First, multiply by -1.
|      1    M+2   -M+1      0      0   2M+3      M      0 -5M+15|

|      0      2      0      0      1      1      1     -1     15|
|      0      1      0      1      0      1      0      0      5|
|      0     -1      1      0      0     -1     -1      1      5| → First, multiply by M-1.
|      1    M+2   -M+1      0      0   2M+3      M      0 -5M+15| ← Second, add to this row.

Done.
            ↓   ↓   ↓               ↓
|   0   2   0   0   1   1   1  -1  15|
|   0   1   0   1   0   1   0   0   5|
|   0  -1   1   0   0  -1  -1   1   5|
|   1   3   0   0   0 M+4   1 M-1  10| ←
```
